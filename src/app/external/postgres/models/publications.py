from ..connection import base
from sqlalchemy import UUID, Column, String, DateTime, ForeignKey, Integer
from datetime import datetime
from sqlalchemy.orm import relationship, Mapped
import uuid


class Raiting(base):
    __tablename__ = "raiting"

    id: Column[UUID] = Column(UUID, default=uuid.uuid4, primary_key=True)
    user_id: Column[UUID] = Column(ForeignKey("users.id"))
    publication_id: Column[UUID] = Column(ForeignKey("publications.id"))
    mark: Column[int] = Column(Integer)


class Publication(base):
    __tablename__ = "publications"

    id: Column[UUID] = Column(UUID, default=uuid.uuid4, primary_key=True)
    publication_text: Column[str] = Column(String, nullable=False)
    publication_date: Column[datetime] = Column(
        DateTime(timezone=True), default=datetime.utcnow
    )
    author_id: Column[UUID] = Column(ForeignKey("users.id"))

    author = relationship("User", uselist=False)
