from ..connection import base
from sqlalchemy import UUID, Column, String
from sqlalchemy.orm import relationship, Mapped
import uuid
from .publications import Publication


class User(base):
    __tablename__ = "users"

    id: Column[UUID] = Column(UUID, default=uuid.uuid4, primary_key=True)
    username: Column[str] = Column(String, unique=True)
    password: Column[str] = Column(String)
