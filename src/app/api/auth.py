from datetime import datetime, timedelta, timezone
from uuid import UUID
from loguru import logger

from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy.ext.asyncio import AsyncSession

from .user.models import UserSchema
from .user.controllers import get_user
from ..settings import settings
from ..external.postgres import get_session

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/user/token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """verify password by check hash

    Args:
        plain_password (str): expected password
        hashed_password (str): hashed password

    Returns:
        bool: true if password verified else false
    """

    return pwd_context.verify(plain_password, hashed_password)


def hash_password(password: str) -> str:
    """hash password

    Args:
        password (str): passwrod

    Returns:
        str: hash of password
    """

    return pwd_context.hash(password)


async def authenticate_user(
    session: AsyncSession, user_schema: OAuth2PasswordRequestForm
) -> UUID:
    """authenticate user

    Args:
        session (AsyncSession): Session of database
        user_schema (OAuth2PasswordRequestForm): Schema

    Raises:
        HTTPException: raise 404 if user not found
        HTTPException: raise 400 if user password incorrect

    Returns:
        UUID: user id
    """

    user_db = await get_user(session, user_schema.username)
    if user_db is None:
        raise HTTPException(404, "user not found")

    if not verify_password(user_schema.password, str(user_db["password"])):
        raise HTTPException(400, "user password incorrect")

    return user_db["id"]


async def create_access_token(data: dict) -> str:
    """create access token

    Args:
        data (dict): data for create token

    Returns:
        str: token
    """

    to_encode = data.copy()
    to_encode["exp"] = datetime.now(timezone.utc) + timedelta(
        minutes=settings.minutes_to_expire_token
    )

    return jwt.encode(to_encode, settings.secret_key, algorithm=settings.algorithm)


async def get_current_user(
    session: AsyncSession = Depends(get_session), token: str = Depends(oauth2_scheme)
) -> UserSchema:
    """get current user

    Args:
        session (AsyncSession, optional): Defaults to Depends(get_session).
        token (str, optional): Defaults to Depends(oauth2_scheme).

    Raises:
        HTTPException: 401 if could nto validate credentials
        HTTPException: 404 if can't find user

    Returns:
        UserSchema: model of user
    """

    try:
        payload = jwt.decode(
            token, settings.secret_key, algorithms=[settings.algorithm]
        )
        username = payload.get("sub", None)
        if username is None:
            raise HTTPException(
                401,
                "could not validate credentials",
            )

    except JWTError:
        raise HTTPException(
            401,
            "could not validate credentials",
        )

    user = await get_user(session, username)
    if user is None:
        logger.error(f'msg="user not found" {username=}')
        raise HTTPException(404, "user not found")

    return UserSchema(
        id=user["id"],
        username=user["username"],
        session=session,
        token=token,
    )
