from datetime import datetime
from uuid import UUID

from pydantic import BaseModel
from enum import Enum


class NewPublicationResponse(BaseModel):
    publication_id: UUID


class PublicationMark(Enum):
    plus = "+"
    minus = "-"


class AuthorResponse(BaseModel):
    id: UUID
    username: str


class PublicationResponse(BaseModel):
    id: UUID
    author_id: UUID
    publication_text: str
    publication_date: datetime
    author: AuthorResponse
    raiting: int
    raiting_count: int


class ErrorMessage(BaseModel):
    detail: str
