from uuid import UUID

from fastapi import APIRouter, Depends

from .controllers import (
    add_new_publication,
    add_voice_for_publication,
    delete_voice_for_publication,
    get_last_publications,
    get_raiting_publications,
)

from ..user.models import UserSchema
from ..auth import get_current_user
from .models import (
    NewPublicationResponse,
    PublicationMark,
    PublicationResponse,
    ErrorMessage,
)


publication_router: APIRouter = APIRouter(
    prefix="/api/publication", tags=["Publication"]
)


@publication_router.post(
    path="",
    name="Add new publication",
    status_code=200,
    response_model=NewPublicationResponse,
)
async def create_new_publication_view(
    text: str, user: UserSchema = Depends(get_current_user)
) -> NewPublicationResponse:
    return NewPublicationResponse(
        publication_id=await add_new_publication(
            text=text, session=user.session, user_id=user.id
        )
    )


@publication_router.put(
    path="/voice",
    name="Add/change voice for publication",
    status_code=204,
    responses={
        400: {
            "description": "Author try to voice for his publications",
            "model": ErrorMessage,
        },
        404: {
            "description": "No publication with such id",
            "model": ErrorMessage,
        },
    },
)
async def voice_for_publication(
    publication_id: UUID,
    mark: PublicationMark,
    user: UserSchema = Depends(get_current_user),
) -> None:
    int_mark = 1 if mark.value == "+" else -1
    await add_voice_for_publication(
        publication_id=publication_id,
        user_id=user.id,
        mark=int_mark,
        session=user.session,
    )


@publication_router.delete(
    path="/voice",
    name="Delete voice",
    status_code=204,
    responses={
        404: {
            "description": "No publication with such id",
            "model": ErrorMessage,
        },
    },
)
async def cancel_voice_for_publication(
    publication_id: UUID,
    user: UserSchema = Depends(get_current_user),
) -> None:
    await delete_voice_for_publication(
        publication_id=publication_id, user_id=user.id, session=user.session
    )


@publication_router.get(
    path="/last",
    name="Get last publications",
    status_code=200,
    response_model=list[PublicationResponse],
)
async def get_last_publications_view(
    publication_count: int = 10, user: UserSchema = Depends(get_current_user)
) -> list[PublicationResponse]:
    return await get_last_publications(publication_count, user.session)


@publication_router.get(
    path="/popular",
    name="Get popular publications",
    status_code=200,
    response_model=list[PublicationResponse],
)
async def get_popular_publications_view(
    offset: int = 0, limit: int = 10, user: UserSchema = Depends(get_current_user)
) -> list[PublicationResponse]:
    return await get_raiting_publications(offset, limit, user.session)
