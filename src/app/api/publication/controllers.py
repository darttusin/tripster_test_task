from uuid import UUID

from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, delete, func, Result
from sqlalchemy.orm import selectinload

from ...external.postgres import Publication, Raiting
from .models import PublicationResponse


async def add_new_publication(text: str, user_id: UUID, session: AsyncSession) -> UUID:
    """Add new publication

    Args:
        text (str): Publication text
        user_id (UUID): User id(author)
        session (AsyncSession): Session of database

    Returns:
        UUID: id of new publication
    """

    publication_model = Publication(publication_text=text, author_id=user_id)

    session.add(publication_model)
    await session.commit()

    await session.refresh(publication_model)
    return UUID(str(publication_model.id))


async def add_voice_for_publication(
    publication_id: UUID, user_id: UUID, mark: int, session: AsyncSession
) -> None:
    """Add voice for publication

    Args:
        publication_id (UUID): Publication id
        user_id (UUID): User id, which want to voice
        mark (int): Mark 1(+) or -1(-)
        session (AsyncSession): Session of database

    Raises:
        HTTPException: 400 if author try to voice for his publications
        HTTPException: 404 if no publication with such id
    """

    # get publication
    stmnt = select(Publication).where(Publication.id == publication_id)
    cursor = await session.execute(stmnt)
    publication = cursor.scalar_one_or_none()

    # check if publication exists
    if publication is None:
        raise HTTPException(404, "no publication with such id")

    # check if user id is not author of publication
    if publication.author_id == user_id:  # type: ignore
        raise HTTPException(400, "author can't voice for his publication")

    # get raiting
    stmnt = select(Raiting).where(
        Raiting.publication_id == publication_id, Raiting.user_id == user_id
    )
    cursor = await session.execute(stmnt)
    raiting = cursor.scalar_one_or_none()

    # if raiting exists, just change mark
    if raiting is not None:
        raiting.mark = mark  # type: ignore
    # else create new raiting
    else:
        raiting = Raiting(user_id=user_id, publication_id=publication_id, mark=mark)

    session.add(raiting)
    await session.commit()


async def delete_voice_for_publication(
    publication_id: UUID, user_id: UUID, session: AsyncSession
) -> None:
    """Delete voice for publications by id

    Args:
        publication_id (UUID): Publication id
        user_id (UUID): User id, which want to delete voice
        session (AsyncSession): Session of database

    Raises:
        HTTPException: Raise 404 if no publication with id
    """

    # check if publication exists
    stmnt = select(Publication).where(Publication.id == publication_id)
    cursor = await session.execute(stmnt)
    publication = cursor.scalar_one_or_none()
    if publication is None:
        raise HTTPException(404, "no publication with such id")

    # delete raiting
    stmnt = delete(Raiting).where(
        Raiting.publication_id == publication_id, Raiting.user_id == user_id
    )
    await session.execute(stmnt)
    await session.commit()


async def __collect_publications(cursor: Result) -> list[PublicationResponse]:
    """Collect publications in one list

    Args:
        cursor (Result): Cursor of database request

    Returns:
        list[PublicationResponse]: List of publications
    """

    publications = []
    for row in cursor.fetchall():
        publication, raiting, raiting_count = row
        publication = publication.__dict__
        publication["author"] = publication["author"].__dict__
        publication["raiting"] = raiting if raiting else 0
        publication["raiting_count"] = raiting_count

        publications.append(PublicationResponse(**publication))

    return publications


async def get_last_publications(
    publication_count: int, session: AsyncSession
) -> list[PublicationResponse]:
    """Get last publications

    Args:
        publication_count (int): Count of publications
        session (AsyncSession): Session for database

    Returns:
        list[PublicationResponse]: List of publications
    """

    stmnt = (
        select(
            Publication,
            func.sum(Raiting.mark).label("raiting"),
            func.count(Raiting.id).label("count"),
        )
        .join(Raiting, Raiting.publication_id == Publication.id, isouter=True)
        .order_by(Publication.publication_date.desc())
        .group_by(Publication)
        .limit(publication_count)
        .options(selectinload(Publication.author))
    )
    return await __collect_publications(await session.execute(stmnt))


async def get_raiting_publications(
    offset: int, limit: int, session: AsyncSession
) -> list[PublicationResponse]:
    """Get raiting publications controller

    Args:
        offset (int): Offset for sql request of publications
        limit (int): Limit for sql request of publications
        session (AsyncSession): Session for database

    Returns:
        list[PublicationResponse]: List of publications
    """

    stmnt = (
        select(
            Publication,
            func.sum(Raiting.mark).label("raiting"),
            func.count(Raiting.id).label("count"),
        )
        .join(Raiting, Raiting.publication_id == Publication.id, isouter=True)
        .order_by(func.sum(Raiting.mark))
        .group_by(Publication)
        .offset(offset)
        .limit(limit)
        .options(selectinload(Publication.author))
    )
    return await __collect_publications(await session.execute(stmnt))
