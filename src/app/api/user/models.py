from uuid import UUID

from pydantic import BaseModel
from sqlalchemy.ext.asyncio import AsyncSession


class RegisterUser(BaseModel):
    username: str
    password: str


class RegisterUserResponse(BaseModel):
    id: UUID
    access_token: str
    token_type: str = "bearer"


class LoginUserResponse(BaseModel):
    id: UUID
    access_token: str
    token_type: str = "bearer"


class UserSchema(BaseModel):
    id: UUID
    username: str
    session: AsyncSession
    token: str

    class Config:
        arbitrary_types_allowed = True


class ErrorMessage(BaseModel):
    detail: str
