from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession

from .models import (
    LoginUserResponse,
    RegisterUser,
    RegisterUserResponse,
    ErrorMessage,
)
from ...external.postgres import get_session
from .controllers import create_user
from ..auth import (
    create_access_token,
    hash_password,
    authenticate_user,
)

user_router: APIRouter = APIRouter(
    prefix="/api/user",
    tags=["User"],
)


@user_router.post(
    path="",
    name="Create user",
    response_model=RegisterUserResponse,
    status_code=200,
    responses={
        409: {"description": "User already exists", "model": ErrorMessage},
    },
)
async def registration(
    user_schema: RegisterUser, session: AsyncSession = Depends(get_session)
) -> RegisterUserResponse:
    user_schema.password = hash_password(user_schema.password)
    user_id = await create_user(session, user_schema)
    access_token = await create_access_token({"sub": user_schema.username})

    return RegisterUserResponse(id=user_id, access_token=access_token)


@user_router.post(
    path="/token",
    name="Get token for user",
    response_model=LoginUserResponse,
    status_code=200,
    responses={
        400: {"description": "User password incorrect", "model": ErrorMessage},
        404: {"description": "User not found", "model": ErrorMessage},
    },
)
async def login(
    user_schema: OAuth2PasswordRequestForm = Depends(),
    session: AsyncSession = Depends(get_session),
) -> LoginUserResponse:
    user_id = await authenticate_user(session, user_schema)
    access_token = await create_access_token({"sub": user_schema.username})

    return LoginUserResponse(id=user_id, access_token=access_token)
