from uuid import UUID

from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio.session import AsyncSession
from sqlalchemy.exc import IntegrityError

from .models import RegisterUser
from ...external.postgres import User


async def create_user(session: AsyncSession, user_schema: RegisterUser) -> UUID:
    """Create new user

    Args:
        session (AsyncSession): Session of database
        user_schema (RegisterUser): Schema of new user

    Raises:
        HTTPException: Raise 409 if username already exists

    Returns:
        UUID: id of new user
    """

    user_model = User(**user_schema.model_dump())

    session.add(user_model)
    try:
        await session.commit()
    except IntegrityError:
        raise HTTPException(409, "username already exists")

    await session.refresh(user_model)
    return UUID(str(user_model.id))


async def get_user(session: AsyncSession, username: str) -> dict | None:
    """Get user for auth

    Args:
        session (AsyncSession): Session of database
        username (str): username

    Returns:
        dict | None: dict if user exists else None
    """

    result = await session.execute(select(User).where(User.username == username))
    result = result.scalar_one_or_none()
    return result.__dict__ if result is not None else None
