from fastapi import APIRouter


ping_router = APIRouter()


@ping_router.get(
    path="/api/ping",
    name="Ping service",
    status_code=200,
)
async def ping_service() -> str:
    return "ok"


@ping_router.get(
    path="/api/healthz",
    name="Service health",
    status_code=200,
)
async def healthz() -> str:
    return "ok"
