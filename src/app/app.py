import time
from typing import Callable, Any

from loguru import logger
from fastapi import FastAPI, Request

from .api.ping import ping_router
from .api.publication.views import publication_router
from .api.user.views import user_router
from .external.postgres import create_tables

app = FastAPI(
    title="Tripster test task",
    version="0.0.1",
    docs_url="/api/docs",
    swagger_ui_parameters={"defaultModelsExpandDepth": -1},  # type: ignore
)


@app.middleware("http")
async def add_process_time_header(request: Request, call_next: Callable) -> Any:
    start_time = time.time()
    response = await call_next(request)

    logger.info(
        f'method="{request.scope["method"]}" router="{request.scope["path"]}" '
        f"process_time={round(time.time() - start_time, 3)} "
        f"status_code={response.status_code}"
    )
    return response


async def startup_handler():
    await create_tables()


app.add_event_handler("startup", startup_handler)

app.include_router(ping_router)
app.include_router(user_router)
app.include_router(publication_router)
