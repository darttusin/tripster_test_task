# Tripster test task

## Запуск апи 

`docker compose up --build -d`

SWAGGER будет доступен по http://127.0.0.1:8000/api/docs

## Запуск тестов 

1. Установка зависимостей `poetry install`

2. В файле .env поменять `db_host` на `db_host="localhost"`

2. Запуск тестов `poetry run pytest`