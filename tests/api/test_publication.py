from fastapi.testclient import TestClient
from sqlalchemy import text
from sqlalchemy.orm import Session


def test_create_user_for_publication(client: TestClient):
    r = client.post(
        "/api/user", json={"username": "testPublication", "password": "pass"}
    )
    assert r.status_code == 200
    global ACCESS_TOKEN
    ACCESS_TOKEN = r.json()["access_token"]


def test_create_publication(client: TestClient):
    r = client.post(
        "/api/publication",
        params={"text": "testPub"},
        headers={"Authorization": f"Bearer {ACCESS_TOKEN}"},
    )
    assert r.status_code == 200
    assert "publication_id" in r.json()
    global PUBLICATION_ID
    PUBLICATION_ID = r.json().get("publication_id")


def test_author_voice_for_publication(client: TestClient):
    r = client.put(
        f"/api/publication/voice?publication_id={PUBLICATION_ID}&mark=%2B",
        headers={"Authorization": f"Bearer {ACCESS_TOKEN}"},
    )
    assert r.status_code == 400


def test_new_user_voice_for_publication(client: TestClient):
    # create 2 user
    r = client.post(
        "/api/user", json={"username": "testPublication2", "password": "pass"}
    )
    assert r.status_code == 200
    global ACCESS_TOKEN2
    ACCESS_TOKEN2 = r.json()["access_token"]

    r = client.put(
        f"/api/publication/voice?publication_id={PUBLICATION_ID}&mark=%2B",
        headers={"Authorization": f"Bearer {ACCESS_TOKEN2}"},
    )
    assert r.status_code == 204


def test_user_voice_without_auth(client: TestClient):
    r = client.put(
        f"/api/publication/voice?publication_id={PUBLICATION_ID}&mark=%2B",
    )
    assert r.status_code == 401


def test_cancel_voice_for_publication(client: TestClient):
    r = client.delete(
        f"/api/publication/voice?publication_id={PUBLICATION_ID}",
        headers={"Authorization": f"Bearer {ACCESS_TOKEN2}"},
    )
    assert r.status_code == 204


def test_last_publications(client: TestClient):
    # create pub for 2 user
    r = client.post(
        url="/api/publication",
        params={"text": "testPub2"},
        headers={"Authorization": f"Bearer {ACCESS_TOKEN2}"},
    )
    assert r.status_code == 200

    # add + for first testPub
    r = client.put(
        f"/api/publication/voice?publication_id={PUBLICATION_ID}&mark=%2B",
        headers={"Authorization": f"Bearer {ACCESS_TOKEN2}"},
    )
    assert r.status_code == 204

    r = client.get(
        "/api/publication/last", headers={"Authorization": f"Bearer {ACCESS_TOKEN2}"}
    )
    assert r.status_code == 200
    assert r.json()[0]["publication_date"] > r.json()[1]["publication_date"]


def test_rating_publications(client: TestClient, session: Session):
    r = client.get(
        "/api/publication/popular", headers={"Authorization": f"Bearer {ACCESS_TOKEN2}"}
    )
    assert r.status_code == 200
    assert r.json()[0]["raiting"] > r.json()[1]["raiting"]

    session.execute(
        text("DELETE FROM raiting; DELETE FROM publications; DELETE FROM users;")
    )
    session.commit()
