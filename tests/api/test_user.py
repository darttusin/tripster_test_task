from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from sqlalchemy import text

USERNAME = "testUser"
PASSWORD = "pass"


def test_create_user(client: TestClient):
    r = client.post("/api/user", json={"username": USERNAME, "password": PASSWORD})
    assert r.status_code == 200
    assert "access_token" in r.json()


def test_create_same_user(client: TestClient):
    r = client.post("/api/user", json={"username": USERNAME, "password": PASSWORD})
    assert r.status_code == 409


def test_login_user(client: TestClient, session: Session):
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    r = client.post(
        "/api/user/token",
        headers=headers,
        data=f"grant_type=&username={USERNAME}&password={PASSWORD}&scope=&client_id=&client_secret=",  # type: ignore
    )
    assert r.status_code == 200
    assert "access_token" in r.json()

    session.execute(text("DELETE FROM users;"))
    session.commit()
