import http from "k6/http";
import { check } from "k6";
import { randomString } from "https://jslib.k6.io/k6-utils/1.1.0/index.js";

const BASE_URL = "http://127.0.0.1:8000";

export const options = {
  scenarios: {
    createUser: {
      executor: 'constant-arrival-rate',
      rate: 10,
      timeUnit: '1s',
      duration: '1m',
      exec: "createUser",
      preAllocatedVUs: 10,
      maxVUs: 20,
  },
  }
}


export async function createUser () {
    const body = {
        "username": randomString(20),
        "password": "string"
    }
    const res = http.post(BASE_URL + "/api/user", JSON.stringify(body));
    
    check(res, { 'status was 200': (r) => r.status == 200 });
}