import http from "k6/http";
import { check } from "k6";
import { randomString } from "https://jslib.k6.io/k6-utils/1.1.0/index.js";

const BASE_URL = "http://127.0.0.1:8000";
const TOKEN = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJzdHJpbmciLCJleHAiOjE3MDc1Nzg4OTF9.ftkY6TgG4N1ghYRaHRf5Vfd1YCt9pEW0BD7lWNG8Ko8";

export const options = {
  scenarios: {
    createPublication: {
      executor: 'constant-arrival-rate',
      rate: 300,
      timeUnit: '1s',
      duration: '1m',
      exec: "createPublication",
      preAllocatedVUs: 300,
      maxVUs: 600,
  },
  }
}


export async function createPublication () {
    const res = http.post(BASE_URL + "/api/publication?text=" + randomString(20), "", {"headers": {"Authorization": TOKEN}});
    
    check(res, { 'status was 200': (r) => r.status == 200 });
}