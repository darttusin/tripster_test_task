from src.app.app import app
from fastapi.testclient import TestClient
import pytest
from sqlalchemy.engine import URL
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
import os


@pytest.fixture(scope="session")
def client():
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope="session")
def session():
    url = URL.create(
        drivername="postgresql",
        username=os.getenv("postgres_username", "api"),
        password=os.getenv("postgres_password", "secretpassw0rd123"),
        host=os.getenv("postgres_host", "localhost"),
        port=int(os.getenv("postgres_port", 5432)),
        database=os.getenv("postgres_database", "api"),
    )
    engine = create_engine(url)

    with Session(engine) as _session:
        yield _session

    engine.dispose()
